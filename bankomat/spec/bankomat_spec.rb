require 'spec_helper'
require 'bankomat'

RSpec.describe 'Bankomat' do
  bankomat = Bankomat.new 100_000, %w[10 50 100 200 500 1000 2000 5000]

  it 'Create bankomat' do
    expect(bankomat.cash).to eq 100_000
    expect(bankomat.numinal).to eq %w[10 50 100 200 500 1000 2000 5000]
  end

  it 'Take maney 100' do
    expect(bankomat.give_maney(100)).to eq 'Take the 100 maney'
  end
  it 'Take maney 700' do
    expect(bankomat.give_maney(700)).to eq 'Take the [500, 200] maney'
  end
  it 'Take maney 1550' do
    expect(bankomat.give_maney(1550)).to eq 'Take the [1000, 500, 50] maney'
  end
  it 'Take maney 10500' do
    expect(bankomat.give_maney(10_500)).to eq 'Take the [5000, 5000, 500] maney'
  end
  it 'Take maney 53750' do
    expect(bankomat.give_maney(53_750)).to eq 'Take the [5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 2000, 1000, 500, 200, 50] maney'
  end
  it 'Take maney 14' do
    expect(bankomat.give_maney(14)).to eq 'Bankomat dont give it you maney'
  end
  it 'Take maney 10 000 000' do
    expect(bankomat.give_maney(10_000_000)).to eq 'No maney, but you hold on'
  end
end
