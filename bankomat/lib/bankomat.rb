class Bankomat
  attr_accessor :cash, :numinal

  def initialize(count, numinal)
    @cash = count
    @numinal = numinal
    puts 'Welcome to Very Right Bank!'
  end

  def give_maney(maney)
    if @cash > maney
      check = @numinal.map(&:to_i)
      puts "Take the #{maney} maney" if check.include? maney
      return "Take the #{maney} maney" if check.include? maney

      unless check.include? maney
        check.delete_if { |n| n > maney }
        cart_maney = []

        rest = maney - check.max
        cart_maney << check.max

        if rest >= check.min
          begin
            while rest > check.max
              rest -= check.max
              cart_maney << check.max
            end
            check.delete_if { |n| n > rest }
          end until check.include? rest
          cart_maney << rest
        else
          puts 'Bankomat dont give it you maney'
          puts "Rest of maney #{rest}"
          return 'Bankomat dont give it you maney'
        end
        puts "Take the #{cart_maney} maney"
        return "Take the #{cart_maney} maney"
      end
    else
      puts "'No maney, but you hold on'"
      'No maney, but you hold on'
    end
  end
end
