class Poker
  RESOULT = ['poker', 'four of a kind', 'three of a kind', 'two pairs', 'one pair', 'all different'].freeze

  def inintialize
    puts 'Game run!'
  end

  def start(numbers)
    array = numbers.to_s.split ''
    check = {}
    array.each do |num|
      check[num] ||= 0
      check[num] += 1
    end
    compute(check.values)
  end

  private

  def compute(array)
    case array.sort!
    when [5] then RESOULT[0]
    when [1, 4] then RESOULT[1]
    when [1, 1, 3] then RESOULT[2]
    when [1, 2, 2] then RESOULT[3]
    when [1, 1, 1, 2] then RESOULT[4]
    when [1, 1, 1, 1, 1] then RESOULT[5]
    else 'error'
    end
  end
end
