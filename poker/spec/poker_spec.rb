require 'spec_helper'
require 'poker'

RSpec.describe Poker do
  describe 'Game RUN' do
    game = Poker.new

    it 'Poker' do
      expect(game.start(55_555)).to eq 'poker'
    end
    it 'four of a kind' do
      expect(game.start(55_155)).to eq 'four of a kind'
    end
    it 'three of a kind' do
      expect(game.start(77_725)).to eq 'three of a kind'
    end
    it 'two pairs' do
      expect(game.start(55_331)).to eq 'two pairs'
    end
    it 'one pair' do
      expect(game.start(55_231)).to eq 'one pair'
    end
    it 'all different' do
      expect(game.start(54_231)).to eq 'all different'
    end
  end
end
